import React from 'react';
import Link from 'next/link';
import Image from 'next/image'


// Page Link 
// 1. import 'Link' component from 'next/link' library
// 2. surround anchor tag with Link tag, for example, link to about page
// <Link href="/"><a>About</a></Link>


// we can use Image component to lazy load the image and get Responsive WD
// 1. import Image component from 'next/image' library
// 2. we have to add width and height attribute in Image component to render image properly such as ~
// <Image src="path" width="width" height="height" />
// 2. to add metadata we import Head component and apply in page component template, for example ~
// import Head from 'next/head'
// <Head>
//   <meta name="" content="">
//   .....
// </Head>

const Navbar = () => (
  <nav>
    <div className="logo">
    {/* Image comp make auto responsive unlike regular img atr + auto lazy load */}
      <Image src="/logo.png"  width={128} height={77}/> 

    </div>
    <Link href="/">
      <a>Home</a>
    </Link>
    <Link href="/about">
      <a>About</a>
    </Link>
    <Link href="/ninjas">
      <a>Ninja Listing</a>
    </Link>
  </nav>
);

export default Navbar;
