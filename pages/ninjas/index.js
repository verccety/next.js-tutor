import React from 'react';
import styles from '../../styles/Ninjas.module.css';
import Link from 'next/link';
// ? STATIC GENERATION
// will run @ build time once. dont write code which works in a browser (like dom)

// Filipe Freire
// getServerSideProps gets executed at every request, while getStaticProps only gets called at build time. When you have data you expect to change every now and then, use the first. When you expect data to stay "static", then getStaticProps is your friend

// ? getStaticProps alternative - getServerSideProps - runs @ request time not build 
export const getStaticProps = async () => {
  const res = await fetch('https://jsonplaceholder.typicode.com/users');
  const data = await res.json();

  return {
    props: {
      ninjas: data,
    },
  };
};

const Ninjas = ({ ninjas }) => {
  return (
    <div>
      <h1>All Ninjas</h1>
      {ninjas.map((ninja) => (
        <Link href={'/ninjas/' + ninja.id} key={ninja.id}>
          <a className={styles.single}>
            <h3>{ninja.name}</h3>
          </a>
        </Link>
      ))}
    </div>
  );
};

export default Ninjas;
