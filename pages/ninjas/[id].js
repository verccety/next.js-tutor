import React from 'react';
/*

If we want to generate dynamic route page components at build time, we use getStaticPaths function to handle the task
1. Inside detail page component (ninjas/[id].js), we invoke getStaticPaths function to fetch remote api to get ninjs list item detail page, example~ 'export const getStaticPaths = async () => { fetch() }
2. To get the path of the id of the ninjas list item, we apply map function and return paths and fallback objects
?  use this to generate pages with dynamic paths @ build time
*/
export const getStaticPaths = async () => {
  const res = await fetch('https://jsonplaceholder.typicode.com/users');
  const data = await res.json();
  const paths = data.map((ninja) => {
    return {
      params: {
        id: ninja.id.toString(),
      },
    };
  });

  return {
    paths,
    fallback: false,
  };
};

/* 
Dynamic Routes (Route Parameter)
Router parameter is the changeable part of the route, for example '.../blogs/blog1', '.../blogs/blog2' where blog1, blog2 is a changeable parameter, like a variable inside a route
1. we create dynamic route component for ninjas page, for example [id].js under pages/ninjas directory. we can name the route whatever we want, id is the most common used name because it is unique
2. If we type '.../ninjas/123', the browser renders the detail page.
3. we want to click the single ninja list item to direct to detail page, so we wrap the list item in ninjas page component. example <Link href={`/ninjas/${ninja.id}`}>...</Link>
4. we can also review this topic in full reactjs tutorial where we apply useParams to handle dynamic route.

*/

// will run for every ninja details page based on paths
// context auto accepted in this func

/* 
 If we want to fetch a single ninja item, we have to apply both getStaticProps and getStaticPaths function inside dynamic route componet ([id].js/detail page component)
Q: Why do we need getStaticPaths function ? 
A: Because we need to tell Nextjs how many html page needed to be made base on our data (remote api)
Then in getStaticProps function we fetch the single item (id) 
and pass the data to detail page component to render the single item content */

export const getStaticProps = async (context) => {
  const id = context.params.id;
  // users/ - на конце слэш. если забыть и не работает
  const res = await fetch('https://jsonplaceholder.typicode.com/users/' + id);
  const data = await res.json();

  return {
    props: {
      ninja: data,
    },
  };
};

function Details({ ninja }) {
  return (
    <div>
      <h1>{ninja?.name}</h1>
      <p>{ninja.email}</p>
      <p>{ninja.website}</p>
      <p>{ninja?.address?.city}</p>
    </div>
  );
}

export default Details;
