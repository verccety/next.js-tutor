import Head from 'next/head';
import Link from 'next/link';
import Footer from '../comps/Footer';
import Navbar from '../comps/Navbar';
import styles from '../styles/Home.module.css';

export default function Home() {
  return (
    <>
      <Head>
      {/* like document.title */}
        <title>Ninja List | Home</title> 
        <meta name="keywords" content="ninjas"/>
      </Head>
      <div>
        <h1 className={styles.title}>Homepage</h1>
        <p className={styles.text}>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste nostrum consectetur cumque voluptatibus necessitatibus nam, error eveniet molestias magnam laborum sit provident ab, libero sint adipisci! Pariatur blanditiis culpa ipsa!
        </p>
        <p className={styles.text}>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste nostrum consectetur cumque voluptatibus necessitatibus nam, error eveniet molestias magnam laborum sit provident ab, libero sint adipisci! Pariatur blanditiis culpa ipsa!
        </p>
        {/* index page - root. other names - equal to that path in url.
        Code splitting enabled by default */}
        <Link href="/ninjas">
          <a className={styles.btn}>See Ninja Listing</a>
        </Link>
      </div>
    </>
  );
}
