import Link from 'next/link';
import React, { useEffect } from 'react';
import { useRouter } from 'next/router';

// Create a customed 404 page
// 1. create 404.jsx (404 is specially named just like index or _app) file under page directory
// 2. import Link module and add link to redirect to designated page component when user mistype url in address bar


// programmatic redirect
// 1. destructure useRouter hook from 'next/router' library
// 2. define a constant 'router' and assign useRouter to it
// 3. within useEffect hook we redirect to home page like router.push('/')

const NotFound = () => {
  const router = useRouter();
  useEffect(() => {
    setTimeout(() => {
      router.push('/'); // router.go(-1) -1 - goes back in history, +1 goes forward
    }, 3000);
  }, []);
  return (
    <div className="not-found">
      <h1>Oooops...</h1>
      <h2>That page cannot be found</h2>
      <p>
        Go back to the{' '}
        <Link href="/">
          <a>Homepage</a>
        </Link>
      </p>
    </div>
  );
};

export default NotFound;
