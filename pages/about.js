import Head from 'next/head';

const About = () => {
  return (
    <>
      <Head>
        {/* like document.title */}
        <title>Ninja List | About</title>
        {/* мета теги, обычно полезны для SEO */}
        <meta name="keywords" content="ninjas" />
      </Head>
      <div>
        <h1>About</h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste nostrum consectetur cumque voluptatibus necessitatibus nam, error eveniet molestias magnam laborum sit provident ab, libero sint adipisci! Pariatur blanditiis culpa ipsa!</p>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Iste nostrum consectetur cumque voluptatibus necessitatibus nam, error eveniet molestias magnam laborum sit provident ab, libero sint adipisci! Pariatur blanditiis culpa ipsa!</p>
      </div>
    </>
  );
};

export default About;
